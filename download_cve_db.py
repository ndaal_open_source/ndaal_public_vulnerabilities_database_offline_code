#!/usr/bin/env python3
# coding: utf-8
# license: Apache 2.0
"""
This script downloads, merges & cleans up the NVD CVE database given
a start year (-s) & an end year (-e) specifiying the range.

Please keep in mind that the years 1998-2001 are included under 2002 in the NVD CVE database.
Therefore, the earliest possible start-date is 2002.

Afterwards, the NVD CVE database is downloaded with vulnerability identifiers
for your specified range, namely:

- Publish Date,
- Modification Date,
- Vulnerability,
- version,
- vector String,
- Attack Vector,
- Attack Complexity,
- Privileges Required,
- User Interaction,
- Scope,
- Confidentiality Impact,
- Integrity Impact,
- Availability Impact.

The following is also extracted from the configurations & description attributes in the data.

- Operating System,
- Attack Type,
- Product,
- Vendor.

Consult download_cve_db.py -h for argument details.

Your final file will be named: CVE_<start_range>_<end_range>.csv
"""
__author__ = "Alaa Jubakhanji"
__organization__ = "ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
__copyright__ = f"Copyright (C) 2021, 2022 {__organization__}"

import io
import time
import zipfile
import argparse
from glob import glob
from pathlib import Path
from datetime import datetime
import requests
import numpy as np
import pandas as pd

parser = argparse.ArgumentParser(
    description="Download the NVD CVE databse for your desired year range.")
parser.add_argument(
    "-s",
    "--start_year",
    type=str,
    help="Start year: first year you want your CVE data to include.",
    required=True)
parser.add_argument(
    "-e",
    "--end_year",
    type=str,
    help="End year: last year you want your CVE data to include.",
    required=True)

args = parser.parse_args()
start_year = args.start_year
end_year = args.end_year


def download_nvd_database(start: int, end: int) -> None:
    # pylint: disable=consider-using-with
    """Download NVD for a specific year range.

    :param start: First year in the download range.
    :type start: int

    :param end: Last year in the download range.
    :type end: int

    :return: None
    :rtype: None
    """
    for year in range(int(start), int(end) + 1):
        url = f"https://nvd.nist.gov/feeds/json/cve/1.1/nvdcve-1.1-{year}.json.zip"
        get = requests.get(f"{url}", timeout=120)
        zip_file = zipfile.ZipFile(
            io.BytesIO(
                get.content))
        zip_file.extractall(".")
        time.sleep(6)


def merge_nvd_database(start: int, end: int) -> None:
    """Merge NVD for a specific year range.

    :param start: First year in the NVD range.
    :type start: int

    :param end: Last year in the NVD range.
    :type end: int

    :return: None
    :rtype: None
    """
    cve_files = glob("*nvdcve-*.json")
    ind_cve = (pd.read_json(f) for f in cve_files)
    full_cve_data = pd.concat(ind_cve, ignore_index=True)
    full_cve_data.to_json(f"CVE_{start}_{end}.json")
    for cve_file in cve_files:
        cve_file_path = Path(cve_file)
        cve_file_path.unlink()


def clean_nvd_database(start: int, end: int) -> object:
    """Clean NVD for a specific year range & transform result to CSV format.

    :param start: First year in the NVD range.
    :type start: int

    :param end: Last year in the NVD range.
    :type end: int

    :return: None
    :rtype: None
    """
    data_frame = pd.read_json(f"CVE_{start}_{end}.json")
    data_frame = pd.json_normalize(data_frame["CVE_Items"])

    cols_to_keep = [
        "publishedDate",
        "lastModifiedDate",
        "cve.CVE_data_meta.ID",
        "impact.baseMetricV3.cvssV3.version",
        "impact.baseMetricV3.cvssV3.vectorString",
        "impact.baseMetricV3.cvssV3.attackVector",
        "impact.baseMetricV3.cvssV3.attackComplexity",
        "impact.baseMetricV3.cvssV3.privilegesRequired",
        "impact.baseMetricV3.cvssV3.userInteraction",
        "impact.baseMetricV3.cvssV3.scope",
        "impact.baseMetricV3.cvssV3.confidentialityImpact",
        "impact.baseMetricV3.cvssV3.integrityImpact",
        "impact.baseMetricV3.cvssV3.availabilityImpact",
        "configurations.nodes",
        "cve.description.description_data"
    ]

    data_frame = data_frame.loc[:, cols_to_keep]

    data_frame.rename(
        columns={
            "publishedDate": "Publish Date",
            "lastModifiedDate": "Modification Date",
            "cve.CVE_data_meta.ID": "Vulnerability",
            "impact.baseMetricV3.cvssV3.version": "Version",
            "impact.baseMetricV3.cvssV3.vectorString": "Vector String",
            "impact.baseMetricV3.cvssV3.attackVector": "Attack Vector",
            "impact.baseMetricV3.cvssV3.attackComplexity": "Attack Complexity",
            "impact.baseMetricV3.cvssV3.privilegesRequired": "Privileges Required",
            "impact.baseMetricV3.cvssV3.userInteraction": "User Interaction",
            "impact.baseMetricV3.cvssV3.scope": "Scope",
            "impact.baseMetricV3.cvssV3.confidentialityImpact": "Confidentiality Impact",
            "impact.baseMetricV3.cvssV3.integrityImpact": "Integrity Impact",
            "impact.baseMetricV3.cvssV3.availabilityImpact": "Availability Impact"},
        inplace=True)

    cols = [
        "Vulnerability",
        "Vector String",
        "Attack Vector",
        "Attack Complexity",
        "Privileges Required",
        "User Interaction",
        "Scope",
        "Confidentiality Impact",
        "Integrity Impact",
        "Availability Impact"]

    data_frame["Publish Date"] = pd.to_datetime(data_frame["Publish Date"])
    data_frame["Modification Date"] = pd.to_datetime(
        data_frame["Modification Date"])
    data_frame["Version"] = data_frame["Version"].astype('float16')
    data_frame[cols] = data_frame[cols].astype("category")

    return data_frame


def prepare_conf_df(start: int, end: int) -> object:
    """Prepare the configurations df for extracting the vendor, product & OS.

    :param start: First year in the NVD range.
    :type start: int

    :param end: Last year in the NVD range.
    :type end: int

    :return confugurations: Full configurations dataframe.
    :rtype: Pandas dataframe

    :return or_df: Slice of configurations dataframe where the operator is OR.
    :rtype: Pandas dataframe

    :return and_df: Slice of configurations dataframe where the operator is AND.
    :rtype: Pandas dataframe
    """
    conf_data_frame = pd.read_json(f"CVE_{start}_{end}.json")
    conf_data_frame = pd.json_normalize(conf_data_frame["CVE_Items"])
    conf_data_frame = conf_data_frame.loc[:, ["configurations.nodes"]]

    conf = list(conf_data_frame["configurations.nodes"])
    conf_data_frame = pd.concat([conf_data_frame.drop(
        "configurations.nodes", axis=1), pd.json_normalize(conf)], axis=1)

    rnge_lst = list(range(1, 680))
    conf_data_frame.drop(columns=rnge_lst, inplace=True)

    conf_data_frame.rename(
        columns={
            0: "configurations"},
        inplace=True)

    configurations = pd.json_normalize(conf_data_frame["configurations"])
    configurations["cpe_match"] = configurations["cpe_match"].astype(str)
    configurations["children"] = configurations["children"].astype(str)

    or_df = configurations[configurations["operator"] == "OR"].copy()
    and_df = configurations[configurations["operator"] == "AND"].copy()

    json_cve_path = Path(f"CVE_{start}_{end}.json")
    json_cve_path.unlink()

    return configurations, or_df, and_df


def extract_vendor(data_frame: object, configurations: object,
                   or_df: object, and_df: object) -> object:
    """Prepare vendor column.

    :param data_frame: Full dataframe.
    :type data_frame: Pandas dataframe

    :param configurations: Full configurations dataframe.
    :type configurations: Pandas dataframe

    :param or_df: Slice of configurations dataframe where the operator is OR.
    :type or_df: Pandas dataframe

    :param and_df: Slice of configurations dataframe where the operator is AND.
    :type and_df: Pandas dataframe

    :return data_frame: Pandas dataframe with the vendor column.
    :rtype: Pandas dataframe
    """
    vendor_re_pattern = [
        "(?<=:o:)(.*?)(?=:)",
        "(?<=:a:)(.*?)(?=:)",
        "(?<=:h:)(.*?)(?=:)"]

    ven_sep_pattern = r"|".join(vendor_re_pattern)

    or_df["Vendor"] = (configurations["cpe_match"].str.extract(
        ven_sep_pattern).stack() .droplevel(1))

    and_df["Vendor"] = (configurations["children"].str.extract(
        ven_sep_pattern).stack() .droplevel(1))

    full_vendor_df = or_df.merge(and_df, on="Vendor", how="left")
    data_frame["Vendor"] = full_vendor_df["Vendor"]

    data_frame["Vendor"] = data_frame["Vendor"].replace('-', np.nan)
    data_frame["Vendor"] = data_frame["Vendor"].replace('*', np.nan)

    data_frame["Vendor"] = data_frame["Vendor"].astype("category")

    return data_frame


def extract_product(data_frame: object, configurations: object,
                    or_df: object, and_df: object) -> object:
    """Prepare product column.

    :param data_frame: Full dataframe.
    :type data_frame: Pandas dataframe

    :param configurations: Full configurations dataframe.
    :type configurations: Pandas dataframe

    :param or_df: Slice of configurations dataframe where the operator is OR.
    :type or_df: Pandas dataframe

    :param and_df: Slice of configurations dataframe where the operator is AND.
    :type and_df: Pandas dataframe

    :return data_frame: Pandas dataframe with the product column.
    :rtype: Pandas dataframe
    """
    # pylint: disable=unnecessary-comprehension
    unique_vendors = [x for x in data_frame["Vendor"].unique()]

    product_re_pattern = []
    for ven in unique_vendors:
        pattern = f"(?<=:{ven}:)(.*?)(?=:)"
        product_re_pattern.append(pattern)

    prod_sep_pattern = r"|".join(product_re_pattern)

    or_df["Product"] = (configurations["cpe_match"].str.extract(
        prod_sep_pattern).stack() .droplevel(1))

    and_df["Product"] = (configurations["children"].str.extract(
        prod_sep_pattern).stack() .droplevel(1))

    data_frame["Product 1"] = or_df["Product"]
    data_frame["Product 2"] = and_df["Product"]

    data_frame["Product"] = data_frame["Product 1"].combine_first(
        data_frame["Product 2"])
    data_frame.drop(columns=["Product 1", "Product 2"], inplace=True)

    data_frame["Product"] = data_frame["Product"].astype("category")

    data_frame["Product"] = data_frame["Product"].replace('-', np.nan)
    data_frame["Product"] = data_frame["Product"].replace('*', np.nan)

    return data_frame


def extract_os(data_frame: object, configurations: object) -> object:
    """Prepare OS column.

    :param data_frame: Full dataframe.
    :type data_frame: Pandas dataframe

    :param configurations: Full configurations dataframe.
    :type configurations: Pandas dataframe

    :return data_frame: Pandas dataframe with the OS column.
    :rtype: Pandas dataframe
    """
    op_sys = [
        "macos",
        "iphone",
        "mac_os_x",
        "linux",
        "windows",
        "android",
        "windows_10",
        "windows_server",
        "windows_11",
        "ios"]

    data_frame["OS"] = configurations["cpe_match"].str.findall(
        "(" + "|".join(op_sys) + ")")
    data_frame["OS"] = data_frame["OS"].apply(lambda x: list(set(x)))

    data_frame = data_frame.explode("OS").reset_index(drop=True)
    data_frame["OS"] = data_frame["OS"].astype("category")

    return data_frame


def extract_attack(data_frame: object) -> object:
    """Prepare attack_type column.

    :param data_frame: Full dataframe.
    :type data_frame: Pandas dataframe

    :return data_frame: Pandas dataframe with the attack_type column.
    :rtype: Pandas dataframe
    """
    # pylint: disable=unnecessary-comprehension
    attack = [x for x in data_frame["cve.description.description_data"]]
    data_frame = pd.concat([data_frame.drop(
        "cve.description.description_data", axis=1), pd.json_normalize(attack)], axis=1)

    generic_attack = [
        "dos",
        "code execution",
        "overflow",
        "memory corruption",
        "bypass",
        "information disclosure",
        "sensitive data exposure",
        "data exposure"
        "privilege escalation",
        "rce",
        "remote code execution",
        "password attack",
        "escalation",
        "pe"]

    owasp_top_10 = [
        "sql injection",
        "sqli",
        "xss",
        "cross site scripting",
        "directory traversal",
        "http response splitting",
        "csrf",
        "cross site request forgery",
        "server side request forgery",
        "ssrf",
        "broken access control",
        "injection",
        "security misconfiguration",
        "broken authentication",
        "insecure seserialization",
        "idor",
        "insecure direct object reference",
        "lfi",
        "local file inclusion",
        "rfi",
        "remote file inclusion",
        "insecure design",
        "server side include injection",
        "vulnerable component",
        "outdated component"
        "authentication failure",
        "identification failure",
        "software failure",
        "data integrity failure",
        "loggin failure",
        "monitoring failure"]

    attack_type = generic_attack + owasp_top_10

    data_frame.rename(
        columns={
            0: "value"},
        inplace=True)

    description = pd.json_normalize(data_frame["value"])

    data_frame.drop(columns=["value"], inplace=True)

    description["value"] = description["value"].astype(str)
    description["value"] = description["value"].str.lower()

    data_frame["Attack Type"] = description["value"].str.findall(
        "(" + "|".join(attack_type) + ")")
    data_frame["Attack Type"] = data_frame["Attack Type"].apply(
        lambda x: list(set(x)))

    data_frame = data_frame.explode("Attack Type").reset_index(drop=True)

    data_frame["Attack Type"].replace(
        owasp_top_10, "OWASP Top 10", inplace=True)

    data_frame["Attack Type"].replace(
        [
            "sensitive information disclosure",
            "information disclosure",
            "sensitive data exposure",
            "data exposure"],
        "Information Disclosure",
        inplace=True)

    data_frame["Attack Type"].replace(["rce",
                                       "code execution",
                                       "remote code execution"],
                                      "Remote Code Execution",
                                      inplace=True)

    data_frame["Attack Type"].replace([
        "pe", "escalation", "privilege escalation"],
        "Privilege Escalation", inplace=True)

    data_frame["Attack Type"] = data_frame["Attack Type"].str.title()

    data_frame["Attack Type"] = data_frame["Attack Type"].astype("category")

    final_cols = [
        "Publish Date",
        "Modification Date",
        "Vulnerability",
        "Attack Type",
        "Version",
        "Vendor",
        "Product",
        "OS",
        "Vector String",
        "Attack Vector",
        "Attack Complexity",
        "Privileges Required",
        "User Interaction",
        "Scope",
        "Confidentiality Impact",
        "Integrity Impact",
        "Availability Impact",
    ]

    data_frame = data_frame.loc[:, final_cols]
    return data_frame


def main() -> None:
    """Provide main functionality of this script."""

    start_time = datetime.now()

    print(" ")
    print(" ")
    print("\x1b[7;30;41m" + " ____ _  _ ____    ___  ___  " + "\x1b[0m")
    print("\x1b[7;30;41m" + " |    |  | |___    |  \\ |__] " + "\x1b[0m")
    print("\x1b[7;30;41m" + " |___  \\/  |___    |__/ |__] " + "\x1b[0m")
    print("\x1b[7;30;41m" + "                             " + "\x1b[0m")
    print("\x1b[7;30;41m" + "         Downloader          " + "\x1b[0m")
    print(" ")
    print("%s" % ("\x1b[7;30;41m" + __organization__ + "\x1b[0m"))
    print(" ")
    print("\33[90m" + str(start_time))
    print(" ")
    print(
        "\x1b[7;30;41m" +
        f"Downloading CVE database from {start_year} to {end_year}..." +
        "\x1b[0m")

    download_nvd_database(start_year, end_year)
    print("\33[90m" + "Download successful")
    print(" ")

    print("\x1b[7;30;41m" + "Preparing data..." + "\x1b[0m")
    merge_nvd_database(start_year, end_year)
    nvd_data_frame = clean_nvd_database(start_year, end_year)
    conf_df, or_op_df, and_op_df = prepare_conf_df(start_year, end_year)
    nvd_data_frame = extract_vendor(
        nvd_data_frame, conf_df, or_op_df, and_op_df)
    nvd_data_frame = extract_product(
        nvd_data_frame, conf_df, or_op_df, and_op_df)
    nvd_data_frame = extract_os(nvd_data_frame, conf_df)
    nvd_data_frame = extract_attack(nvd_data_frame)
    nvd_data_frame.to_csv(f"CVE_{start_year}_{end_year}.csv", index=False)
    end_time = datetime.now()
    print("\33[90m" + f"Done, process took: {str(end_time - start_time)}")


if __name__ == "__main__":
    main()
